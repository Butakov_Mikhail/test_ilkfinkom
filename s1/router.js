var router = require('express').Router();
module.exports = router;

/**Функция логгирования запросов**/
function rlog(req,res,next){
	var method=Object.keys(req.route.methods)[0].toUpperCase();
	console.log('[ROUTE]',method,req.route.path,'URL:'+req.url);
    next();
}

/**Привязка адресов и обработчиков**/
/* регистрация */
router.get('/reg', rlog, require('./page/reg'));
router.get('/api/regUser', rlog, require('./api/regUser'));
/* логин */
router.get('/login', rlog, require('./page/login'));
router.get('/api/checkUser', rlog, require('./api/checkUser'));