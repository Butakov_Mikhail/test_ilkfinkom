var amqp = require('amqplib/callback_api');

module.exports=function(sendArgs,recCb){
	amqp.connect('amqp://localhost', function(err, conn){
		conn.createChannel(function(err, ch){
			ch.assertQueue('', {exclusive: true}, function(err, q){
				var corr = "qweqwe"+Math.random()+Math.random()+Math.random();
				//получение
				ch.consume(q.queue, function(msg){
					if (msg.properties.correlationId == corr){
						var recieveData=msg.content.toString()
                  console.log('[RABBIT] Получено',recieveData);
                  //обработка
						recCb(JSON.parse(recieveData));
						conn.close();
					}
				}, {noAck: true});
				//отправка
				var sendData=JSON.stringify(sendArgs);
				ch.sendToQueue(
					'rpc_queue', 
					new Buffer(sendData), 
					{correlationId: corr, replyTo: q.queue}
				);
				console.log('[RABBIT] Отправлено', sendArgs);
			});
		});
	});
}