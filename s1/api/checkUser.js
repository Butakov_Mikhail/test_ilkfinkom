var rabbit=require('../rabbit.js');
module.exports=function(req,res){
	if(!req.query.login)
		return res.json({success:false, message:'Введите логин'});
	if(!req.query.password)
		return res.json({success:false, message:'Введите пароль'});
	rabbit(
		{operation:'getUser',data:{login:req.query.login}},
		function(args){
			if(args.success==false)
				return res.json({success:false, message:'Ошибка базы'});
			if(args.data.length<1)
				return res.json({success:false, message:'Пользователь не зарегистрирован'});
			
			var user=args.data[0];
			if(user.password!==req.query.password)
				return res.json({success:false, message:'Неверный пароль'});
			return res.json({success:true, message:'Вход прошела успешно'});
		});
}