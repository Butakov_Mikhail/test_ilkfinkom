var rabbit=require('../rabbit.js');
module.exports=function(req,res){
	if(!req.query.login)
		return res.json({success:false, message:'Введите логин'});
	if(!req.query.password)
		return res.json({success:false, message:'Введите пароль'});
	rabbit(
		{operation:'addUser',data:{login:req.query.login,password:req.query.password}},
		function(args){
			if(args.success==false)
				return res.json({success:false, message:'Ошибка базы'});
			return res.json({success:true, message:'Регистрация прошла успешно'});
		});	
}