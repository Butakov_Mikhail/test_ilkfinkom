var express = require('express'),
    app = express(),
    swig=require('swig'),
	compression = require('compression');

module.exports=function(config){
	var port = config.port || process.env.PORT || 3000,
        router=config.router,
        errorHandler=config.error,
        publicDir=config.public,
        viewDir=config.view;

	app.use(compression({level: 9}));
    if(publicDir) app.use(express.static(publicDir));
    
    app.engine('html', swig.renderFile);
    app.set('views', viewDir);
    app.set('view engine', 'html');
    app.set('view cache', true);

    app.use('/',router);
    app.all('*',function(req, res, next) {
		var err=new Error('Not Found');
		err.status=404;
		next(err);
	});
	app.use(function(err,req,res,next){
		if(err){
			err.status=err.status||500;
			err.message=err.message||'Internal Server Error';
			if(errorHandler)return errorHandler(err,req,res,next);
			if(err.status==404)
				console.log('[ROUTER] URL:',req.url,err.message);
			else
				console.error('[SERVER ERROR]',err.stack);
			res.status(err.status);
			res.send(err.message);
		}
	});
	app.listen(port);
	console.log('[SERVER] Server started on port',port);
	return app;
};