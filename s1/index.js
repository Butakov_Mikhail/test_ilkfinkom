var app = require('./http_server.js')({
    port: 3000,
    router: require('./router.js'),
	public: __dirname + '/public',
	view: __dirname + '/view'
});