require('./rabbit.js')(function(args,cb){
	var handler;
	switch(args['operation']){
		case 'addUser': 
			handler=require('./api/addUser');
			break;
		case 'getUser':
			handler=require('./api/getUser');
			break;
		default:
			handler=function(args,cb){
				cb({success:false, message:'Unknown operation'});
			}
	}
	handler(args,cb);
});