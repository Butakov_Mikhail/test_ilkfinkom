module.exports=function(args,cb){
	if(!args.data)
		return cb({success:false,message:"Data not found"});
	if(!args.data.login)
		return cb({success:false,message:"Login not found"});
	if(!args.data.password)
		return cb({success:false,message:"Password not found"});
	var db=require('../db.js'),
		login=args.data.login,
		password=args.data.password;

	//можно бы и в хэш, но в тз умолчали
	db('users')
		.insert({login:login,password:password})
		.returning('*')
		.then(function(rows){
			cb({success:true,message:"User ["+login+"] created",data:rows});
		})
		.catch(function(err){
			cb({success:false,message:"User creation error: "+err.detail});
		});
}