module.exports=function(args,cb){
	if(!args.data)
		return cb({success:false,message:"Data not found"});
	if(!args.data.login)
		return cb({success:false,message:"Login not found"});
	var db=require('../db.js'),
		login=args.data.login;

	db('users')
		.select('*')
		.where('login',login)
		.then(function(rows){
			cb({success:true,message:"User ["+login+"] selected",data:rows});
		});
}