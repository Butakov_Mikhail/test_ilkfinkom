-- Database: test_ilkfinkom

DROP DATABASE IF EXISTS test_ilkfinkom;

--создание базы
CREATE DATABASE test_ilkfinkom;
WITH OWNER = postgres
	ENCODING = 'UTF8'
	TABLESPACE = pg_default
	LC_COLLATE = 'ru_RU.UTF-8'
	LC_CTYPE = 'ru_RU.UTF-8'
	CONNECTION LIMIT = -1;

--DROP TABLE IF EXISTS Users;
--создание таблицы
CREATE TABLE Users
(
	id BIGSERIAL PRIMARY KEY,
	login VARCHAR(64) NOT NULL UNIQUE,
	password VARCHAR(64) NOT NULL
)