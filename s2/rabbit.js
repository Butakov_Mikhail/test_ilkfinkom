var amqp = require('amqplib/callback_api');

module.exports=function(reqHandler){
	amqp.connect('amqp://localhost', function(err, conn){
		conn.createChannel(function(err, ch){
			var q = 'rpc_queue';

			ch.assertQueue(q, {durable: false});
			ch.prefetch(1);
			//получение
			ch.consume(q, function(msg){
				var recieveData = JSON.parse(msg.content.toString());
				console.log('[RABBIT] Получено',recieveData);
				//обработка
				reqHandler(recieveData,function(sendArgs){
					var sendData=JSON.stringify(sendArgs);
					//отправка
					ch.sendToQueue(
						msg.properties.replyTo, 
						new Buffer(sendData),
						{correlationId: msg.properties.correlationId}
					);
					console.log('[RABBIT] Отправлено',sendData);
					ch.ack(msg);
				})
			});
		});
	});
}